FROM node:10
WORKDIR /usr/app
COPY . . 
RUN npm install
RUN npm install -g pm2
EXPOSE 3000
CMD ["/bin/sh", "-c", "pm2-runtime 'npm start'"]
